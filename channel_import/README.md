Whirlwind install of Channel Import


## Video

https://vimeo.com/510491999

## Prerequisite tools

* Kafkacat
* Docker
* Smile CDR 2021.02
* Insomnia

## Topics

* What is Channel Import
* Setting up a local message broker (kafka)
* Setting up channel import. 
* Processing FHIR JSON
* Processing non-FHIR JSON
* Processing arbitrary Strings
* Processing CSV files




