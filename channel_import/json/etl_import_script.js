function handleChannelImportJsonPayload(jsonObject) {
   var name = jsonObject.name;
   var parts = name.split(' ')

   //Create an empty patient
   var patient = ResourceBuilder.build('Patient');
   //Populate it with a name.
   patient.name[0].given[0] = parts[0];
   patient.name[0].family = parts[1];

   //Create the transaction
   var transaction = TransactionBuilder.newTransactionBuilder();
   //Add the patient create operation to the transaction
   transaction.create(patient);
   //Execute the transaction
   Fhir.transaction(transaction);
}

