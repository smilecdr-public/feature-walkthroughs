function handleEtlImportRow(inputMap, context) {
  var patient = ResourceBuilder.build('Patient');
  var name = inputMap['name'];
  var id = inputMap['id'];
  patient.name[0].family = name;
  var tx = TransactionBuilder.newTransactionBuilder();
  tx.create(patient);
  Fhir.transaction(tx);
}
