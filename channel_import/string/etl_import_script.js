function handleChannelImportStringPayload(stringPayload) {
   var prefix = "Hello my name is ";
   var name = stringPayload.substring(prefix.length);

   //Create an empty patient
   var patient = ResourceBuilder.build('Patient');
   //Populate it with a name.
   patient.name[0].given[0] = name

   //Create the transaction
   var transaction = TransactionBuilder.newTransactionBuilder();
   //Add the patient create operation to the transaction
   transaction.create(patient);
   //Execute the transaction
   Fhir.transaction(transaction);
}

